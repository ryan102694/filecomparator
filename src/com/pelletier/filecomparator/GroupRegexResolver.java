package com.pelletier.filecomparator;

import java.util.List;
import java.util.regex.Matcher;
/**
 * Sometimes regex must be made very specific in order to capture the accepted file differences. 
 * The problem with this is that the regex may match differences that are in fact errors, and ignore them. 
 * This resolver allows the user to provide a list of regex group indexes (which start at 1, not 0). By doing this 
 * the user can specify which groups in the regex match are not allowed to have differences.
 * 
 *  <br><b>NOTE:</b> Cannot use optional groups
 * 
 * @author Ryan Pelletier
 *
 */
public class GroupRegexResolver extends RegexResolver {

	/**
	 * Indexes of regex group matches which are not allowed to differ between the baseline and the test case.
	 * For example, with the regex pattern "([0-9]{1})([A-Z]{1})", used on the string 3G, there will be two groups, 
	 * one for 3, one for G. If the user specifies groupsInRegexMatchThatCantBeDifferent to include only 2, the difference 
	 * between 3G and 4G would be resolved, but 3G and 3H would not.
	 */
	List<Integer> groupsInRegexMatchThatCantBeDifferent = null;
	//, delimited list
	/**
	 * If regex matches, checks to see if any of the regex groups are different that have an index
	 * specified by groupsInRegexMatchThatCantBeDifferent, if so, sets error on FileDifference to true, otherwise,
	 * set error to false.
	 * 
	 * If no matches are found, do nothing. 
	 */
	@Override
	public void resolve(FileDifference fileDifference) {

		Matcher matcherOriginal = pattern.matcher(fileDifference.getDelta().getOriginal().getLines().get(0).toString());
		Matcher matcherRevised = pattern.matcher(fileDifference.getDelta().getRevised().getLines().get(0).toString());

		if ((matcherOriginal.find() && matcherRevised.find())) {
			for (int i = 1; i <= matcherOriginal.groupCount(); i++) {
				if(groupsInRegexMatchThatCantBeDifferent.contains(i) && !matcherOriginal.group(i).equals(matcherRevised.group(i))){
					fileDifference.setIsError(true);
					return;
				}
			}
			if(fileDifference.getIsError() == null){
				fileDifference.setIsError(false);
			}
		} 
	}

	public void setGroupsInRegexMatchThatCantBeDifferent(List<Integer> groupsInRegexMatchThatCantBeDifferent) {
		this.groupsInRegexMatchThatCantBeDifferent = groupsInRegexMatchThatCantBeDifferent;
	}
}
