package com.pelletier.filecomparator;
/**
 * Should be put last in the chain, marks unresolved fileDifferences as errors
 * @author Ryan Pelletier
 *
 */
public class ErrorMarkerResolver implements Resolver {

	
	@Override
	public void resolve(FileDifference fileDifference) {
		if(fileDifference.getIsError() == null){
			fileDifference.setIsError(true);
		}		
	}

}
