package com.pelletier.filecomparator;

import difflib.Delta;
/**
 * Essentially adds an error field for a Delta object so we can either resolve the 
 * fileDifference or not.
 * 
 * @author Ryan Pelletier
 */
public class FileDifference {
	
	private Delta delta = null;
	private Boolean isError = null;	//true if this difference is an error we care about

	public FileDifference(){}

	public void setDelta(Delta delta) {
		this.delta = delta;
	}

	public Delta getDelta() {
		return delta;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}
	
	@Override
	public String toString(){
		return delta.toString();	//can customize this later
	}
	
}
