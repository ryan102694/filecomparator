package com.pelletier.filecomparator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pelletier.filecomparator.testUtil.FileDifferenceUtil;
import com.pelletier.filecomparator.testUtil.MatcherUtil;
/**
 * TestNG case for comparing two files, uses matcherUtil to pair files with same name in directory, 
 * then uses fileDifferenceUtil to calculate differences and resolve differences for any configured resolvers.
 * 
 * @author Ryan Pelletier
 *
 */
public class FileComparator {

	FileDifferenceUtil fileDifferenceUtil;
	MatcherUtil matcherUtil;

	@DataProvider(name = "files")
	public Iterator<Object[]> pairFiles() {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		matcherUtil = (MatcherUtil) context.getBean("matcherUtil");
		fileDifferenceUtil = (FileDifferenceUtil) context.getBean("fileDifferenceUtil");
		
		String baseline = System.getenv("BASELINE");	
		String different = System.getenv("DIFFERENT");	
		String path = System.getenv("BASEDIR");

		if(path != null){
			matcherUtil.setAbsoluteDir(path);
		}
		if (baseline != null) {
			matcherUtil.setBaseDir(baseline);
		}
		if (different != null) {
			matcherUtil.setDiffDir(different);
		}
		
		return matcherUtil.pairFiles().iterator();
	}

	@Test(dataProvider = "files")
	public void compareFiles(File baselineFile, File testFile)throws FileNotFoundException, IOException {		
		List<FileDifference> fileDifferences = fileDifferenceUtil.getFileDifferencesAfterResolving(baselineFile.getAbsolutePath(),testFile.getAbsolutePath());

		for(FileDifference fileDifference : fileDifferences){
			if(fileDifference.getIsError()){
				Assert.assertEquals(fileDifference.getDelta().getOriginal().getLines().get(0), fileDifference.getDelta().getRevised().getLines().get(0));
			}
		}
	}

	public void setFileDifferenceUtil(FileDifferenceUtil fileDifferenceUtil) {
		this.fileDifferenceUtil = fileDifferenceUtil;
	}

	public void setMatcherUtil(MatcherUtil matcherUtil) {
		this.matcherUtil = matcherUtil;
	}
	
}
