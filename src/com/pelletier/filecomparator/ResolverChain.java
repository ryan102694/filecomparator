package com.pelletier.filecomparator;

import java.util.List;
/**
 * Chain of resolvers. For a list of file differences attempts to resolve the differences
 * in the files to either marked as error, or not an error. If resolvers in 
 * the chain can not confirm or deny an error, differences are marked as error.
 * @author Ryan Pelletier
 *
 */

public class ResolverChain {
	/**
	 * List of resolvers that will run in the chain.
	 */
	private List<Resolver> resolvers = null; 

	public void resolve(List<FileDifference> fileDifferences) {
		for (FileDifference fileDifference : fileDifferences) {
			for (Resolver resolver : resolvers) {
				if (fileDifference.getIsError() == null) {
					resolver.resolve(fileDifference);
				}else{
					//break back to next fileDifference
					break;
				}
			}
		}
	}

	public void setResolvers(List<Resolver> resolvers) {
		this.resolvers = resolvers;
	}

	public List<Resolver> getResolvers() {
		return resolvers;
	}

}
