package com.pelletier.filecomparator;


/**
 * Used in resolver chain, if resolver can conclude that a FileDifference is either an error
 * or not, it should set the error on the FileDifference to true or false. 
 * 
 * If no conclusion can be made, do nothing.
 * @author Ryan Pelletier
 *
 */
public interface Resolver {	

	public void resolve(FileDifference fileDifference);
	
}
