package com.pelletier.filecomparator.testUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.testng.collections.CollectionUtils;

import difflib.DiffUtils;
import difflib.Patch;

/**
 * This utility examines two directories and pairs the files up by name.
 * Used by FileComparator in the DataProvider to supply pairs of files to the TestNG case.
 * @author Ryan Pelletier
 *
 */
public class MatcherUtil {
	private String baseDir = null;
	private String diffDir = null;
	private String absoluteDir = null;
	
	public MatcherUtil(){}
	
	public Set<Object[]> pairFiles(){
		return match(this.baseDir, this.diffDir, this.absoluteDir);
	}
	
	public Set<Object[]> pairFiles(String baseDir, String diffDir, String absoluteDir){
		return match(baseDir, diffDir, absoluteDir);
	}
	
	/**
	 * 
	 * @param baseDir
	 * Name of directory that contains the baseline file of the pair.
	 * @param diffDir
	 * Name of directory that contains the non-baseline file of the pair.
	 * @return
	 */
	private Set<Object[]> match(String baseDir, String diffDir, String absoluteDir) {

		if(!absoluteDir.endsWith("/")){
			absoluteDir += '/';
		}
		Map<String, File> baseline = getUniqueNames(new File(absoluteDir + baseDir));
		Map<String, File> results = getUniqueNames(new File(absoluteDir + diffDir));

		Set<String> union = new TreeSet<String>();
		union.addAll(baseline.keySet());
		union.addAll(results.keySet());

		Set<Object[]> result = new HashSet<Object[]>();
		for (String key : union) {
			File baselineFile = baseline.get(key);
			File resultsFile = results.get(key);
			result.add(new File[] { baselineFile, resultsFile });
		}
		return result;
	}
	/**
	 * 
	 * @param directory
	 * Name of directory to find unique files in 
	 * @return
	 * Map, key is unique name of file relative to directory, value is actual File object
	 */
	public Map<String, File> getUniqueNames(File directory) {
		if (!directory.isDirectory()) {
			directory = directory.getParentFile();
		}
		Map<String, File> map = new HashMap<String, File>();
		String absolutePath = directory.getAbsolutePath();		
		List<File> baselineFiles = (List<File>) FileUtils.listFiles(directory,TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		for (File file : baselineFiles) {
			String uniqueName = file.getAbsolutePath().replace(absolutePath, "");
			map.put(uniqueName, file);
		}
		return map;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}
	public void setDiffDir(String diffDir) {
		this.diffDir = diffDir;
	}

	public void setAbsoluteDir(String absoluteDir) {
		this.absoluteDir = absoluteDir;
	}	

}
