package com.pelletier.filecomparator.testUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.pelletier.filecomparator.FileDifference;
import com.pelletier.filecomparator.ResolverChain;

import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;

/**
 * Uses difflib to find differences between files, can either return differences between
 * files, or differences after running the injected resolverChain.
 * @author Ryan Pelletier
 *
 */
public class FileDifferenceUtil {

	/**
	 * Resolver chain used to resolve fileDifferences.
	 */
	ResolverChain resolverChain = null;

	public FileDifferenceUtil() {}

	/**
	 * 
	 * @param baselineFile
	 * Name of file used as a baseline.
	 * @param resultFile
	 * Name of file to be compared against the baseline file.
	 * @return
	 * FileDifferences between the baseline file and result file after using chain to resolve differences.
	 */
	public List<FileDifference> getFileDifferencesAfterResolving(String baselineFile, String resultFile){
		List<FileDifference> fileDifferences = getFileDifferences(baselineFile, resultFile);
		resolverChain.resolve(fileDifferences);
		return fileDifferences;
	}
	/**
	 * 
	 * @param baselineFile
	 * Name of file used as a baseline.
	 * @param resultFile
	 * Name of file to be compared against the baseline file.
	 * @return
	 * FileDifferences between the baseline file and result file
	 */
	public List<FileDifference> getFileDifferences(String baselineFile, String resultFile){
		List<String> original = fileToLines(baselineFile);
		List<String> revised = fileToLines(resultFile);
		
		Patch patch = DiffUtils.diff(original, revised);
		
		List<FileDifference> fileDifferences = new ArrayList<FileDifference>();
		for (Delta delta : (List<Delta>) patch.getDeltas()) {
			FileDifference fileDifference = new FileDifference();	//take Delta instead
			fileDifference.setDelta(delta);
			fileDifferences.add(fileDifference);
		}
		
		return fileDifferences;
	}
	/**
	 * 
	 * @param filename
	 * Name of file to use
	 * @return
	 * List of lines in the file
	 */
	private List<String> fileToLines(String filename) {
		List<String> lines = new LinkedList<String>();
		String line = "";
		try {
			BufferedReader in = new BufferedReader(new FileReader(filename));
			while ((line = in.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lines;
	}

	public void setResolverChain(ResolverChain resolverChain) {
		this.resolverChain = resolverChain;
	}
}
