package com.pelletier.filecomparator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

import difflib.ChangeDelta;
import difflib.Chunk;

public class MultipleLineResolverTest {
	
	FileDifference fileDifference = null;
	List<String> baseLines = null;
	List<String> diffLines = null;
	MultipleLineResolver multipleLineResolver = null;
	
	@Before
	public void setup(){
		fileDifference = new FileDifference();
		baseLines = new ArrayList<String>();
		diffLines = new ArrayList<String>();
		multipleLineResolver = new MultipleLineResolver();
	}
	
	@Test
	public void testNotSettingErrorWhenFileDifferenceHasMultipleLines() {
		baseLines.add(new String("here is a line"));
		diffLines.add(new String("here is a line"));
		
		fileDifference.setDelta(new ChangeDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));
		
		Assert.assertEquals(fileDifference.getIsError(), null);
		multipleLineResolver.resolve(fileDifference);
		Assert.assertEquals(fileDifference.getIsError(), null);
	}

	
	@Test
	public void testSettingErrorWhenFileDifferenceHasMultipleLines() {
		baseLines.add(new String("here is a line"));
		diffLines.add(new String("here is a line"));
		diffLines.add(new String("here is another line"));
		diffLines.add(new String("even one more line"));
		
		fileDifference.setDelta(new ChangeDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));
		
		Assert.assertEquals(fileDifference.getIsError(), null);
		multipleLineResolver.resolve(fileDifference);
		Assert.assertEquals(fileDifference.getIsError(), new Boolean(true));

	}

}
