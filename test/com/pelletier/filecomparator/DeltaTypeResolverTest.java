package com.pelletier.filecomparator;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;
import difflib.ChangeDelta;
import difflib.Chunk;
import difflib.DeleteDelta;
import difflib.Delta.TYPE;
import difflib.InsertDelta;

public class DeltaTypeResolverTest {
	
	DeltaTypeResolver preliminaryResolver = null;
	FileDifference fileDifference = null;
	
	List<String> diffLines = null;
	List<String> baseLines = null;
	
	List<TYPE> types = null;
	TYPE changeType = TYPE.CHANGE;
	TYPE deleteType = TYPE.DELETE;
	TYPE insertType = TYPE.INSERT;
	
	
	@Before
	public void setup(){
	
		diffLines = new ArrayList<String>();
		diffLines.add(new String("here is a mocked file line (difference)"));
		baseLines = new ArrayList<String>();
		baseLines.add(new String("here is a mocked file line"));
		
		fileDifference = new FileDifference();
		
		types = new ArrayList<TYPE>();
		preliminaryResolver = new DeltaTypeResolver();
	}
	
	@Test
	public void testNotCatchingDelta(){
		fileDifference.setDelta(new InsertDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));
		types.add(changeType);
		types.add(deleteType);
		preliminaryResolver.setAutomaticErrorTypes(types);
		preliminaryResolver.resolve(fileDifference);
		Assert.assertEquals(fileDifference.getIsError(), null);	
	}

	@Test
	public void testCatchingInsertDelta() {
		fileDifference.setDelta(new InsertDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));
		types.add(insertType);
		preliminaryResolver.setAutomaticErrorTypes(types);
		preliminaryResolver.resolve(fileDifference);
		Assert.assertEquals(fileDifference.getIsError(), new Boolean(true));
	}
	
	@Test
	public void testCatchingDeleteDelta(){
		fileDifference.setDelta(new DeleteDelta<String>(new Chunk<String>(0,baseLines), new Chunk<String>(0,diffLines)));
		types.add(deleteType);
		preliminaryResolver.setAutomaticErrorTypes(types);
		preliminaryResolver.resolve(fileDifference);
		Assert.assertEquals(fileDifference.getIsError(), new Boolean(true));
	}
	
	@Test 
	public void testChangingChangeDelta(){	//probably won't ever need to catch this in the real world
		fileDifference.setDelta(new ChangeDelta<String>(new Chunk<String>(0,baseLines), new Chunk<String>(0,diffLines)));
		types.add(changeType);
		preliminaryResolver.setAutomaticErrorTypes(types);
		preliminaryResolver.resolve(fileDifference);
		Assert.assertEquals(fileDifference.getIsError(), new Boolean(true));
	}
	
}
