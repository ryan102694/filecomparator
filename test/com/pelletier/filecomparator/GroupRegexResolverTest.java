package com.pelletier.filecomparator;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;
import difflib.ChangeDelta;
import difflib.Chunk;

public class GroupRegexResolverTest {
	//fileDifference
	
	FileDifference fileDifference = null;
	GroupRegexResolver groupRegexResolver = null;
	List<Integer> groupsInRegexMatchThatCantBeDifferent = null;
	List<String> diffLines = null;
	List<String> baseLines = null;
	String pattern = null;
	
	@Before
	public void setup(){
		fileDifference = new FileDifference();
		groupRegexResolver = new GroupRegexResolver();
		groupsInRegexMatchThatCantBeDifferent = new ArrayList<Integer>();
	}

	@Test
	public void testCatchingAnUnallowedDifferenceAmongAllowedDifferences() {
		//regex groups that are allowed to be different
		groupsInRegexMatchThatCantBeDifferent.add(1);
		
		//line differences
		diffLines = new ArrayList<String>();
		diffLines.add(new String("DE RXFBC #1234 1234568"));
		baseLines = new ArrayList<String>();
		baseLines.add(new String("DE RXFBA #5235 1114568"));
		
		
		fileDifference = new FileDifference();
		fileDifference.setDelta(new ChangeDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));
				
		groupRegexResolver.setPattern("(DE [A-Z]{5} )(#[0-9]{4})( [0-9]{7})");
		groupRegexResolver.setGroupsInRegexMatchThatCantBeDifferent(groupsInRegexMatchThatCantBeDifferent);
		groupRegexResolver.resolve(fileDifference);
		
		Assert.assertEquals(new Boolean(true), fileDifference.getIsError());
	}
	
	@Test
	public void testIgnoringAllowedDifferences(){
		//regex groups that are allowed to be different
		groupsInRegexMatchThatCantBeDifferent.add(4);
		
		//line differences
		diffLines = new ArrayList<String>();
		diffLines.add(new String("ABCD"));
		baseLines = new ArrayList<String>();
		baseLines.add(new String("ZRFD"));
		
		
		fileDifference = new FileDifference();
		fileDifference.setDelta(new ChangeDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));

		groupRegexResolver.setPattern("([A-Z]{1})([A-Z]{1})([A-Z]{1})([A-Z]{1})");
		groupRegexResolver.setGroupsInRegexMatchThatCantBeDifferent(groupsInRegexMatchThatCantBeDifferent);
		groupRegexResolver.resolve(fileDifference);
		
		Assert.assertFalse(fileDifference.getIsError(), "An error was not expected.");
	}
	
	@Test
	public void testNotFindingRegexMatchAndIgnoringFileDifference(){
		//regex groups that are allowed to be different
		groupsInRegexMatchThatCantBeDifferent.add(4);
		
		//line differences
		diffLines = new ArrayList<String>();
		diffLines.add(new String("ABCD"));
		baseLines = new ArrayList<String>();
		baseLines.add(new String("ZRFD"));
		
		
		fileDifference = new FileDifference();
		fileDifference.setDelta(new ChangeDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));

		groupRegexResolver.setPattern("([1-9]{1})([1-9]{1})([1-9]{1})([1-9]{1})");//no match found
		groupRegexResolver.setGroupsInRegexMatchThatCantBeDifferent(groupsInRegexMatchThatCantBeDifferent);
		groupRegexResolver.resolve(fileDifference);
		
		Assert.assertNull(fileDifference.getIsError(), "Error was not null.");
	}
	
	@Test
	public void testWithNoGroupsInRegexMatchThatCantBeDifferent(){
		//line differences
		diffLines = new ArrayList<String>();
		diffLines.add(new String("ABC"));
		baseLines = new ArrayList<String>();
		baseLines.add(new String("EFGF"));
		
		fileDifference = new FileDifference();
		fileDifference.setDelta(new ChangeDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));

		groupRegexResolver.setPattern("([A-Z]{1})?([A-Z]{1})([A-Z]{1})([A-Z]{1})");//no match found
		groupRegexResolver.setGroupsInRegexMatchThatCantBeDifferent(groupsInRegexMatchThatCantBeDifferent);
		groupRegexResolver.resolve(fileDifference);
		
		Assert.assertEquals(new Boolean(false), fileDifference.getIsError());
	}
}
