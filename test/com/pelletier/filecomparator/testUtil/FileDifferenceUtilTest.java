package com.pelletier.filecomparator.testUtil;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.pelletier.filecomparator.FileDifference;
import com.pelletier.filecomparator.RegexResolver;
import com.pelletier.filecomparator.Resolver;
import com.pelletier.filecomparator.ResolverChain;
import com.pelletier.filecomparator.testUtil.FileDifferenceUtil;

public class FileDifferenceUtilTest {

	FileDifferenceUtil fileDifferenceUtil = null;
	ResolverChain resolverChain = null;
	List<Resolver> resolvers = null;
	
	@Before
	public void setup(){

		RegexResolver dtgResolver  = new RegexResolver();
		dtgResolver.setPattern("(Z [0-9]{6})");
		
		RegexResolver ssnResolver = new RegexResolver();
		ssnResolver.setPattern("(#[0-9]{4})");
		
		List<Resolver> resolvers = new ArrayList<Resolver>();
		resolvers.add(dtgResolver);
		resolvers.add(ssnResolver);
		
		resolverChain = new ResolverChain();
		resolverChain.setResolvers(resolvers);
		
		fileDifferenceUtil = new FileDifferenceUtil();
		fileDifferenceUtil.setResolverChain(resolverChain);
	}

	@Test
	public void testFindDifference() {
		List<FileDifference> fileDifferences = fileDifferenceUtil.getFileDifferences("testDirectories/testDirectory/testBaseDir_1/test.txt", "testDirectories/testDirectory/testDiffDir_1/test.txt");
		
		assertEquals(fileDifferences.size(),1);
		assertEquals(fileDifferences.get(0).getDelta().getOriginal().toString(),"[position: 27, size: 1, lines: [#1234]]");
		assertEquals(fileDifferences.get(0).getIsError(), null);
	}
	
	@Test
	public void testFindResolvedDifference() {
		List<FileDifference> fileDifferences = fileDifferenceUtil.getFileDifferencesAfterResolving("testDirectories/testDirectory/testBaseDir_1/test.txt", "testDirectories/testDirectory/testDiffDir_1/test.txt");
		
		assertEquals(fileDifferences.size(),1);
		assertEquals(fileDifferences.get(0).getDelta().getOriginal().toString(),"[position: 27, size: 1, lines: [#1234]]");
		assertEquals(fileDifferences.get(0).getIsError(), false);
	}
	
	@Test
	public void testFindingMultipleDifferencesWithResolution(){
		List<FileDifference> fileDifferences = fileDifferenceUtil.getFileDifferencesAfterResolving("testDirectories/testDirectory/testBaseDir_2/subdir/test.txt", "testDirectories/testDirectory/testDiffDir_2/subdir/test.txt");
		
		assertEquals(fileDifferences.size(),2);
		assertEquals(fileDifferences.get(0).getDelta().getOriginal().toString(),"[position: 12, size: 1, lines: [Z 231134Z SEP 05 ZYH]]");
		assertEquals(fileDifferences.get(0).getIsError(), false);
		assertEquals(fileDifferences.get(1).getIsError(), null);
	}


}
