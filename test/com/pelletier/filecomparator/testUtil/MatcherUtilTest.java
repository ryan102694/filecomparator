package com.pelletier.filecomparator.testUtil;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

import org.junit.Test;
import org.testng.Assert;

public class MatcherUtilTest {
	//test getting files where they are unique

	//test case for sub-directories still same
	@Test
	public void testFindingPairsInSubdirectories() {
		MatcherUtil matcherUtil= new MatcherUtil();
		Iterator<Object[]> pairs = matcherUtil.pairFiles("testDirectory/testBaseDir_2","testDirectory/testDiffDir_2", "testDirectories/").iterator();
		Object[] pair = pairs.next();
		Object[] pair2 = pairs.next();
		
		assertEquals("testDirectories\\testDirectory\\testBaseDir_2\\hello.txt", pair2[0].toString());
		assertEquals("testDirectories\\testDirectory\\testDiffDir_2\\hello.txt", pair2[1].toString());
		assertEquals("testDirectories\\testDirectory\\testBaseDir_2\\subdir\\test.txt", pair[0].toString());
		assertEquals("testDirectories\\testDirectory\\testDiffDir_2\\subdir\\test.txt", pair[1].toString());
	}
	
	@Test
	public void testGetUniqueNames(){
		MatcherUtil matcherUtil = new MatcherUtil();
		Map<String,File> map = matcherUtil.getUniqueNames(new File("testDirectories/testDirectory\\date_1"));
		File file1 = map.get("\\protocolA\\id_001\\format\\test.txt");
		File file2 = map.get("\\protocolB\\id_001\\format\\test.txt");
		Assert.assertEquals(new File("testDirectories/testDirectory/date_1/protocolA/id_001/format/test.txt"), file1);
		Assert.assertEquals( new File("testDirectories/testDirectory/date_1/protocolB/id_001/format/test.txt"), file2);
	}
	
	//basic test case for matching two files
	@Test
	public void testFindingBasicPair() {
		MatcherUtil matcherUtil= new MatcherUtil();
		Iterator<Object[]> pairs = matcherUtil.pairFiles("testDirectory/testBaseDir_1","testDirectory/testDiffDir_1", "testDirectories/").iterator();
		Object[] pair = pairs.next();
		assertEquals("testDirectories\\testDirectory\\testBaseDir_1\\test.txt", pair[0].toString());
		assertEquals("testDirectories\\testDirectory\\testDiffDir_1\\test.txt", pair[1].toString());
	}

	//test case for extra/missing file
	@Test
	public void testFindingBasicPairWithMissingFile() {
		MatcherUtil matcherUtil= new MatcherUtil();
		Iterator<Object[]> pairs = matcherUtil.pairFiles("testDirectory/testBaseDir_3","testDirectory/testDiffDir_3", "testDirectories/").iterator();
		Object[] pair = pairs.next();
		Object[] pair2 = pairs.next();
		
		assertEquals("testDirectories\\testDirectory\\testBaseDir_3\\test2.txt", pair2[0].toString());
		assertEquals(null, pair2[1]);
		assertEquals("testDirectories\\testDirectory\\testBaseDir_3\\test1.txt", pair[0].toString());
		assertEquals("testDirectories\\testDirectory\\testDiffDir_3\\test1.txt", pair[1].toString());
	}
	
	//test case for extra/missing file in sub-directory
	@Test
	public void finalTest(){
		MatcherUtil matcherUtil = new MatcherUtil();
		Iterator<Object[]> pairs = matcherUtil.pairFiles("testDirectory/testBaseDir_4","testDirectory/testDiffDir_4", "testDirectories").iterator();
		Object[] pair = pairs.next();
		Object[] pair2 = pairs.next();
		
		assertEquals(null, pair2[0]);
		assertEquals("testDirectories\\testDirectory\\testDiffDir_4\\subdir\\test2.txt", pair2[1].toString());
		assertEquals("testDirectories\\testDirectory\\testBaseDir_4\\subdir\\test.txt", pair[0].toString());
		assertEquals("testDirectories\\testDirectory\\testDiffDir_4\\subdir\\test.txt", pair[1].toString());
	}

}
