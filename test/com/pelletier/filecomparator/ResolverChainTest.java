package com.pelletier.filecomparator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

import difflib.ChangeDelta;
import difflib.Chunk;
import difflib.DeleteDelta;
import difflib.InsertDelta;
import difflib.Delta.TYPE;

public class ResolverChainTest {
	//a few different resolvers
	RegexResolver regexResolver1 = null;
	RegexResolver regexResolver2 = null;
	DeltaTypeResolver preliminaryResolver = null;
	MultipleLineResolver multipleLineResolver = null;
	
	//a few different fileDifferences
	FileDifference fileDifference1 = null;
	FileDifference fileDifference2 = null;
	
	List<FileDifference> fileDifferences = null;
	List<Resolver> resolverList = null;
	ResolverChain resolverChain = null;
	
	//lines with different text to test resolvers
	List<String> diffLines = null;
	List<String> baseLines = null;
	
	//Delta types
	List<TYPE> types = null;
	TYPE changeType = TYPE.CHANGE;
	TYPE deleteType = TYPE.DELETE;
	TYPE insertType = TYPE.INSERT;
	
	@Before
	public void setup(){
		baseLines = new ArrayList<String>();
		diffLines = new ArrayList<String>();
		
		regexResolver1 = new RegexResolver();
		regexResolver2 = new RegexResolver();
		preliminaryResolver = new DeltaTypeResolver();
		multipleLineResolver = new MultipleLineResolver();
		
		types = new ArrayList<TYPE>();
		types.add(insertType);
		types.add(deleteType);
		preliminaryResolver.setAutomaticErrorTypes(types);
		
		fileDifference1 = new FileDifference();
		fileDifference2 = new FileDifference();
		
		fileDifferences = new ArrayList<FileDifference>();
		resolverList = new ArrayList<Resolver>();
		resolverChain = new ResolverChain();
	}
	
	@Test
	public void testWhileUsingMultipleLineResolver(){
		baseLines.add(new String("a line"));
		diffLines.add(new String("a delta with two lines"));
		diffLines.add(new String("a delta with two lines"));

		
		// file differences
		fileDifference1.setDelta(new InsertDelta<String>(new Chunk<String>(0,baseLines), new Chunk<String>(0, diffLines)));
		fileDifferences.add(fileDifference1);
		
		resolverList.add(multipleLineResolver);
		regexResolver1.setPattern("(delta)");
		resolverList.add(regexResolver1);
		resolverChain.setResolvers(resolverList);
		
		resolverChain.resolve(fileDifferences);
		/*
		 * In this case, the regex resolver would usually mark the file 
		 * difference as not an error. However we have a multiple line resolver,
		 * which will mark the difference as an error, and the regex resolver will
		 * never even attempt to resolve the file difference.
		 */
		
		Assert.assertEquals(new Boolean(true), fileDifference1.getIsError());
		
	}
	
	
	@Test
	public void testWhileUsingDeltaTypeResolver() {
		baseLines.add(new String("a few lines"));
		diffLines.add(new String("a few lines difference"));
		// file differences
		fileDifference1.setDelta(new InsertDelta<String>(new Chunk<String>(0,baseLines), new Chunk<String>(0, diffLines)));
		fileDifference2.setDelta(new DeleteDelta<String>(new Chunk<String>(0,baseLines), new Chunk<String>(0, diffLines)));
		fileDifferences.add(fileDifference1);
		fileDifferences.add(fileDifference2);

		
		resolverList.add(preliminaryResolver);
		regexResolver1.setPattern("(difference)");
		resolverList.add(regexResolver1);
		resolverChain.setResolvers(resolverList);
		
		resolverChain.resolve(fileDifferences);
		/*
		 * DeltaTypeResolver catches InsertDelta and DeleteDelta and marks it as an error before the
		 * regex resolver can mark the difference as not an error
		 * 
		 * This is just making sure that after a preliminary resolver sees a certain
		 * type of delta the other resolvers don't also try to resolve
		 */
		Assert.assertEquals(new Boolean(true), fileDifference1.getIsError());
		Assert.assertEquals(new Boolean(true), fileDifference2.getIsError());

	}
	
	@Test
	public void testResolvingSomeFileDifferences(){
		baseLines.add(new String("a few lines"));
		diffLines.add(new String("a few lines difference"));
		

		// file differences
		fileDifference1.setDelta(new ChangeDelta<String>(new Chunk<String>(0,baseLines), new Chunk<String>(0, baseLines)));

		fileDifference2.setDelta(new ChangeDelta<String>(new Chunk<String>(0,baseLines), new Chunk<String>(0, diffLines)));
		fileDifferences.add(fileDifference1);
		fileDifferences.add(fileDifference2);

		// resolver chain
		regexResolver1.setPattern("wont match");
		regexResolver2.setPattern("(difference)");

		resolverList.add(regexResolver1);
		resolverList.add(regexResolver2);
		resolverChain.setResolvers(resolverList);
		
		resolverChain.resolve(fileDifferences);
		
		//fileDifference1 was not resolved, fileDifference2 was
		Assert.assertEquals(null, fileDifference1.getIsError());
		Assert.assertEquals(new Boolean(false), fileDifference2.getIsError());

	}
	
	@Test
	public void testResolvingErrorsInChain() {
		// lines
		baseLines.add(new String("line"));
		diffLines.add(new String("another line"));

		// file differences
		fileDifference1.setDelta(new ChangeDelta<String>(new Chunk<String>(0,baseLines), new Chunk<String>(0, diffLines)));
		fileDifference2.setDelta(new ChangeDelta<String>(new Chunk<String>(0,baseLines), new Chunk<String>(0, diffLines)));
		fileDifferences.add(fileDifference1);
		fileDifferences.add(fileDifference2);

		// resolver chain
		regexResolver1.setPattern("wont match");
		regexResolver2.setPattern("(another)");

		resolverList.add(regexResolver1);
		resolverList.add(regexResolver2);
		resolverChain.setResolvers(resolverList);
		
		resolverChain.resolve(fileDifferences);
		for(FileDifference fileDifference : fileDifferences){
			Assert.assertEquals(new Boolean(false), fileDifference.getIsError());
		}
	}

	@Test
	public void testRunningChainWithNothingResolved(){
		//lines
		baseLines.add(new String("line"));
		diffLines.add(new String("another line"));
		
		//file differences
		fileDifference1.setDelta(new ChangeDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));
		fileDifference2.setDelta(new ChangeDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));
		fileDifferences.add(fileDifference1);
		fileDifferences.add(fileDifference2);
		
		//resolver chain
		regexResolver1.setPattern("some regex");
		regexResolver2.setPattern("more regex");
		
		resolverList.add(regexResolver1);
		resolverList.add(regexResolver2);
		resolverChain.setResolvers(resolverList);
		
		//resolve
		resolverChain.resolve(fileDifferences);
		
		//check that all differences are  errors (no matches were found)
		for(FileDifference fileDifference : fileDifferences){
			Assert.assertEquals(null, fileDifference.getIsError());
		}
	}

}
